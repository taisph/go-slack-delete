package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

type FilesUploadResponse struct {
	Ok    bool   `json:"ok"`
	Error string `json:"error"`
}

type FilesListResponse struct {
	Ok     bool   `json:"ok"`
	Error  string `json:"error"`
	Files  []File `json:"files"`
	Paging Paging `json:"paging"`
}

type FilesDeleteResponse struct {
	Ok    bool   `json:"ok"`
	Error string `json:"error"`
}

type File struct {
	Id        string `json:"id"`
	Created   int    `json:"created"`
	Timestamp int    `json:"timestamp"`
	Name      string `json:"name"`
	Title     string `json:"title"`
	Size      int    `json:"size"`
}

type Paging struct {
	Count int `json:"count"`
	Total int `json:"total"`
	Page  int `json:"page"`
	Pages int `json:"pages"`
}

var webapi_url string = "https://slack.com/api/"
var token string = ""

func main() {

	ts := time.Now().AddDate(0, -3, 0)
	fl, err := list(ts)
	if err != nil {
		panic(fmt.Sprintf("Error retrieving file list: %v", err))
	}

	sz := len(fl)
	for i := range fl {
		fmt.Printf("Deleting file %d/%d: %s\n", i+1, sz, fl[i].Name)
		err := delete(fl[i].Id)
		if err != nil {
			fmt.Printf("Error deleting file: %v\n", err)
		}
	}
}

func delete(id string) error {
	uv := url.Values{}
	uv.Add("file", id)

	body, err := doRequest("files.delete", uv)
	if err != nil {
		return fmt.Errorf("%v", err)
	}

	v := new(FilesDeleteResponse)
	err = json.Unmarshal(body, v)
	if err != nil {
		return fmt.Errorf("%v", err)
	}

	if !v.Ok {
		return fmt.Errorf("%v", v.Error)
	}

	return nil
}

func list(to time.Time) ([]File, error) {
	page := 0
	pages := 0
	perpage := 100

	var fl []File

	for page <= pages {
		page++

		uv := url.Values{}
		uv.Add("ts_to", strconv.FormatInt(to.Unix(), 10))
		uv.Add("count", strconv.Itoa(perpage))
		uv.Add("page", strconv.Itoa(page))

		body, err := doRequest("files.list", uv)
		if err != nil {
			return nil, fmt.Errorf("%v", err)
		}

		v := new(FilesListResponse)
		err = json.Unmarshal(body, v)
		if err != nil {
			return nil, fmt.Errorf("%v", err)
		}

		if !v.Ok {
			return nil, fmt.Errorf("%v", v.Error)
		}

		if pages == 0 {
			pages = v.Paging.Pages
		}

		for i := range v.Files {
			fl = append(fl, v.Files[i])
		}
	}

	return fl, nil
}

func doRequest(ep string, uv url.Values) ([]byte, error) {
	uv.Add("token", token)

	cl := &http.Client{}
	req, err := http.NewRequest("GET", webapi_url+ep, nil)
	if err != nil {
		return nil, fmt.Errorf("%v", err)
	}
	req.URL.RawQuery = uv.Encode()
	res, err := cl.Do(req)
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("%v", err)
	}
	return body, nil
}
